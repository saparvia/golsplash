// @license magnet:?xt=urn:btih:90dc5c0be029de84e523b9b3922520e79e0e6f08&dn=cc0.txt CC0

// Simple splash screen playing the Game of Life

// To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.

// Some predefined patterns
var PATTERNS = {
	'gosper_glider_gun': [[24, 0], [22, 1], [24, 1], [12, 2], [13, 2], [20, 2], [21, 2], [34, 2], [35, 2], [11, 3], [15, 3], [20, 3], [21, 3], [34, 3], [35, 3], [0, 4], [1, 4], [10, 4], [16, 4], [20, 4], [21, 4], [0, 5], [1, 5], [10, 5], [14, 5], [16, 5], [17, 5], [22, 5], [24, 5], [10, 6], [16, 6], [24, 6], [11, 7], [15, 7], [12, 8], [13, 8]],
	'centinal': [[0, 0], [1, 0], [50, 0], [51, 0], [1, 1], [50, 1], [1, 2], [3, 2], [25, 2], [26, 2], [48, 2], [50, 2], [2, 3], [3, 3], [12, 3], [25, 3], [26, 3], [39, 3], [40, 3], [48, 3], [49, 3], [11, 4], [12, 4], [39, 4], [41, 4], [10, 5], [11, 5], [41, 5], [11, 6], [12, 6], [15, 6], [16, 6], [39, 6], [40, 6], [41, 6], [11, 10], [12, 10], [15, 10], [16, 10], [39, 10], [40, 10], [41, 10], [10, 11], [11, 11], [41, 11], [11, 12], [12, 12], [39, 12], [41, 12], [2, 13], [3, 13], [12, 13], [25, 13], [26, 13], [39, 13], [40, 13], [48, 13], [49, 13], [1, 14], [3, 14], [25, 14], [26, 14], [48, 14], [50, 14], [1, 15], [50, 15], [0, 16], [1, 16], [50, 16], [51, 16]]
}

var GOLSplash = function(str='Please wait...', pattern_name='centinal') {
	this.cellsx = 64;
	this.cellsy = 64;

	this.padding = 10;
	this.canvas_elem = document.createElement('canvas');

	this.canvas_elem.style.position = 'absolute';
	this.canvas_elem.style.left = this.padding+'px';
	this.canvas_elem.style.top = this.padding+'px';
	this.canvas_elem.style.margin = '0px';

	this.canvas_elem.style.background = 'white';

	var body = document.getElementsByTagName('body')[0]
	body.appendChild(this.canvas_elem);


	this.text = document.createElement('h1')
	this.text.innerHTML = str;
	this.text.style.position = 'absolute';
	this.text.style.margin = 'auto';
	this.text.style.transform = 'translate(-50%,50%)';
	body.appendChild(this.text)

	this.canvas = this.canvas_elem.getContext('2d');

	this.cells = [];
	for (var i=0; i<this.cellsx; i++) {
		this.cells[i] = [];
		for (var j=0; j<this.cellsy; j++) {
			this.cells[i][j] = 0;
		}
	}
	var cells = this.cells;

	var pattern = PATTERNS[pattern_name];
	pattern.forEach(function(point) {
		cells[point[0]][point[1]] = 1;
	});

}

GOLSplash.prototype.update = function () {
	var result = [];

	var cells = this.cells;
    
	function count_neighbours(x, y) {
		var count = 0;
        
		function filled(x, y) {
			return cells[x] && cells[x][y];
		}
        
		if (filled(x-1, y-1)) count++;
		if (filled(x, y-1)) count++;
		if (filled(x+1, y-1)) count++;
		if (filled(x-1, y)) count++;
		if (filled(x+1, y)) count++;
		if (filled(x-1, y+1)) count++;
		if (filled(x, y+1)) count++;
		if (filled(x+1, y+1)) count++;
        
		return count;
	}
    
	this.cells.forEach(function(row, x) {
		result[x] = [];
		row.forEach(function(cell, y) {
			var state = 0;
			count = count_neighbours(x, y);

			if (cell > 0) {
				state = (count == 2 || count == 3)
			} else {
				state = count == 3;
			}
		    
			result[x][y] = state;
		});
	});
    
	this.cells = result;
    
	this.update_size();
	this.draw();
	window.addEventListener('resize', this.update_size);
};

GOLSplash.prototype.update_size = function() {
	this.canvas_elem.style.width = window.innerWidth-2*this.padding;
	this.canvas_elem.style.height = window.innerHeight-2*this.padding;
	this.text.style.left = window.innerWidth/2;
	this.text.style.top = window.innerHeight/2;

	this.canvas_elem.setAttribute('width', this.canvas_elem.offsetWidth);
	this.canvas_elem.setAttribute('height', this.canvas_elem.offsetHeight);

	this.canvas.strokeStyle = '#e1e1e1'; // For some reason these need to be reset every time (at least on Firefox)
	this.canvas.fillStyle = 'cadetblue';
}

GOLSplash.prototype.draw = function() {
	var cellwidth = this.canvas_elem.getAttribute('width')/this.cellsx;
	var cellheight = this.canvas_elem.getAttribute('height')/this.cellsy;
	var canvas = this.canvas;

	canvas.clearRect(0, 0, this.cellwidth*this.cellsx, this.cellheight*this.cellsy);
	this.cells.forEach(function(row, x) {
		row.forEach(function(cell, y) {
			canvas.beginPath();
			canvas.rect(x*cellwidth, y*cellheight, cellwidth, cellheight);
			if (cell) {
				canvas.fill();
			} else {
				canvas.stroke();
			}
		});
	});
	splash = this;
	setTimeout(function() { splash.update(); }, 70);
}

GOLSplash.prototype.close = function() {
	this.canvas_elem.parentElement.removeChild(this.canvas_elem);
	this.text.parentElement.removeChild(this.text);
}

// @license-end
