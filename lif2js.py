# Convert a Life 1.06 pattern file to a javascript array

import sys

f = sys.stdin

coords = []
for line in f:
    if line.startswith('#'): continue
    coords.append([int(x) for x in line.split()])
    
minx = 0
miny = 0
for x,y in coords:
    if x < minx: minx = x
    if y < miny: miny = y

for coord in coords:
    coord[0] -= minx
    coord[1] -= miny

print('[%s]'%', '.join([str(coord) for coord in coords]))
